"""
Python list model
"""
from datetime import date
from .Model import Model

class model(Model):
    def __init__(self):
        self.ReviewsList = []

    def select(self):
        """
        Returns guestentries list of lists
        Each list in guestentries contains: name, email, date, message
        :return: List of lists
        """
        return self.ReviewsList

    def insert(self, Department, CourseNumber, Stars, Instructor, Year, Comment):
        """
        Appends a new list of values representing new message into guestentries
        :param name: String
        :param email: String
        :param message: String
        :return: True
        """
        params = [Department, CourseNumber, Stars, Instructor, Year, Comment]
        self.ReviewsList.append(params)
        return True
