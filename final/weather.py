import requests, json
from flask import redirect,request,url_for,render_template
from flask.views import MethodView

Temp=None
Humidity=None
Pressure=None
Type=None
Description=None
city_name=None

class Weather(MethodView):
    #Funtion to pass in data from post to the frontend.
    def get(self):
            return render_template('Home_Page.html', city_name=city_name, Temp=Temp, Humidity=Humidity, Pressure=Pressure, Description=Description) 

    #Function 
    def post(self):
        api_key = "9c3ecb904db5857f209ade70b135e8cd" #Store API KEY
        base_url = "http://api.openweathermap.org/data/2.5/weather?" #Store API Endpoint

        city_name = request.form['CITY'] #Get the city name form the frontend.

        complete_url = base_url + "appid=" + api_key + "&q=" + city_name #Makes a complete url
        response = requests.get(complete_url) #Make the get request to Weather API
        x = response.json() #Store it as a JSON file.
        #Checks if city name exits
        if x["cod"] != "404":
            #Parsing JSON
                response= x["main"]

                Temp= response["temp"]
                Humidity= response["humidity"]
                Pressure= response["pressure"]
                Type= x["weather"]
                Description= Type[0]["description"]
        #Render the templatee and Save the data.
        return render_template('Home_Page.html', city_name=city_name, Temp=Temp, Humidity=Humidity, Pressure=Pressure, Description=Description) 

