import flask
from flask.views import MethodView
from weather import Weather

app = flask.Flask(__name__)       # our Flask app

app.add_url_rule('/',
                 view_func=Weather.as_view('weather'),
                 methods=["GET","POST"])

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True)
