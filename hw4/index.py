from flask import render_template
from flask.views import MethodView
import gbmodel

class Index(MethodView):
    def get(self):
        model = gbmodel.get_model()
        review = [dict(Department=row[0], CourseNumber=row[1], Stars=row[2], Instructor=row[3], Year=row[4], Comment=row[5]) for row in model.select()]
        return render_template('index.html',review=review)
