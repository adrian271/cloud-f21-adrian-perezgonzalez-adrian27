"""
A simple guestbook flask app.
Data is stored in a SQLite database that looks something like the following:

+------------+------------------+------------+----------------+
| Name       | Email            | signed_on  | message        |
+============+==================+============+----------------+
| John Doe   | jdoe@example.com | 2012-05-28 | Hello world    |
+------------+------------------+------------+----------------+

This can be created with the following SQL (see bottom of this file):

    create table guestbook (name text, email text, signed_on date, message);

"""
from datetime import date
from .Model import Model
import sqlite3
DB_FILE = 'ClassReview.db'    # file for our Database

class model(Model):
    def __init__(self):
        # Make sure our database exists
        connection = sqlite3.connect(DB_FILE)
        cursor = connection.cursor()
        try:
            cursor.execute("select count(rowid) from ClassReview")
        except sqlite3.OperationalError:
            cursor.execute("create table ClassReview (Department, CourseNumber, Stars, Instructor, Year, Comment)")
        cursor.close()

    def select(self):
        """
        Gets all rows from the database
        Each row contains: name, email, date, message
        :return: List of lists containing all rows of database
        """
        connection = sqlite3.connect(DB_FILE)
        cursor = connection.cursor()
        cursor.execute("SELECT * FROM ClassReview")
        return cursor.fetchall()

    def insert(self, Department, CourseNumber, Stars, Instructor, Year, Comment):
        """
        Inserts entry into database
        :param name: String
        :param email: String
        :param message: String
        :return: True
        :raises: Database errors on connection and insertion
        """
        params = {'Department':Department, 'CourseNumber':CourseNumber, 'Stars':Stars, 'Instructor':Instructor, 'Year':Year, 'Comment':Comment}
        connection = sqlite3.connect(DB_FILE)
        cursor = connection.cursor()
        cursor.execute("insert into ClassReview (Department, CourseNumber, Stars, Instructor, Year, Comment) VALUES (:Department, :CourseNumber, :Stars, :Instructor, :Year, :Comment)", params)

        connection.commit()
        cursor.close()
        return True
