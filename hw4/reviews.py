from flask import redirect, request, url_for, render_template
from flask.views import MethodView
import gbmodel

class Reviews(MethodView):
    def get(self):
        return render_template('reviews.html')

    def post(self):
        """
        Accepts POST requests, and processes the form;
        Redirect to index when completed.
        """
        model = gbmodel.get_model()
        model.insert(request.form['Department'], request.form['CourseNumber'], request.form['Stars'], request.form['Instructor'], request.form['Year'], request.form['Comment'])
        return redirect(url_for('index'))
